﻿using System;
using System.IO;
using System.Text;
using System.Web;
using azure_second.Backend;

namespace azure_second
{
    public partial class DavaiRabotai : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void UploadFile( object sender, EventArgs eventArgs)
        {
            HttpPostedFile file = Request.Files["uploading_file"];

            var loader = new StorageLoader();
            loader.UploadFiles( file );
        }

        public void DownloadFile(object sender, EventArgs eventArgs)
        {
            var obj = Request.Form["download_file_name"];
            
            new StorageLoader().DownloadFile( obj, Response );
        }
    }
}