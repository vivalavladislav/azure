﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace azure_second.Backend
{
    public class StorageLoader
    {
        private const string c_CONNECTION_STRING = "ConnectionBalalekshn";
        
        private CloudStorageAccount _account;
        private CloudBlobContainer _container;


        public StorageLoader()
        {
            string connectionString = null;
            /*
            foreach (var str in ConfigurationManager.ConnectionStrings)
            {
                Console.WriteLine( str );
            }
            var connection = ConfigurationManager.ConnectionStrings[c_CONNECTION_STRING];
            connectionString = connection.ConnectionString;
            */
            connectionString =
                "DefaultEndpointsProtocol=https;AccountName=sunduchokvladislava;AccountKey=IDZDcmrprqe7U9Y3Hu4C8h1vh1TLUbieiCw63FyoP0JDocjT3LkI/a1ywiez96BxoA/2IK8DZJu5yfiwN3oIhg==";
            //connectionString = @"DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==";
            //connectionString = "UseDevelopmentStorage=true";
            _account = CloudStorageAccount.Parse(connectionString);

            CloudBlobClient client = _account.CreateCloudBlobClient();

            _container = client.GetContainerReference("svalka-of-useless-files");
            try
            {
                _container.CreateIfNotExists();
            }
            catch (StorageException ex)
            {
                Console.Write(ex.ToString());
            }

            // setting permissions
            var permissions = new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob };
            _container.SetPermissions(permissions);
        }


        public void UploadFiles( HttpPostedFile file )
        {
            CloudBlockBlob targetBlob = _container.GetBlockBlobReference( file.FileName );

            targetBlob.UploadFromStream( file.InputStream );

            foreach( var blob in _container.ListBlobs())
            {
                Console.WriteLine( blob.Uri );
            }
        }

        public void DownloadFile(string fileName, HttpResponse response )
        {
            var blob = _container.GetBlockBlobReference(fileName);
            if (blob == null)
            {
                return;
            }
            /*
            response.ContentType = "image/jpeg";
            response.AppendHeader("Content-Disposition", "attachment; filename=SailBig.jpg");
            response.TransmitFile( blob.Uri.ToString() );
            //Response.TransmitFile(Server.MapPath("~/images/sailbig.jpg"));
            response.End();
             * */
            /*
            using (var downloadingFile = File.OpenWrite( "D:/"+ blob.Name))
            {
                blob.DownloadToStream( downloadingFile );
            }*/


            Stream stream = null;
            int bytesToRead = blob.StreamWriteSizeInBytes;

            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Create a WebRequest to get the file
                var fileReq = (HttpWebRequest)WebRequest.Create(blob.Uri);

                //Create a response for this request
                var fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }
        }
    }
}