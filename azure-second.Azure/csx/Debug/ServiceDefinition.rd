﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="azure_second.Azure" generation="1" functional="0" release="0" Id="5848a729-96a4-45de-9d65-7faecbe0506f" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="azure_second.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="azure-second:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/azure_second.Azure/azure_second.AzureGroup/LB:azure-second:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="azure-second:StorageConnection" defaultValue="">
          <maps>
            <mapMoniker name="/azure_second.Azure/azure_second.AzureGroup/Mapazure-second:StorageConnection" />
          </maps>
        </aCS>
        <aCS name="azure-secondInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/azure_second.Azure/azure_second.AzureGroup/Mapazure-secondInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:azure-second:Endpoint1">
          <toPorts>
            <inPortMoniker name="/azure_second.Azure/azure_second.AzureGroup/azure-second/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="Mapazure-second:StorageConnection" kind="Identity">
          <setting>
            <aCSMoniker name="/azure_second.Azure/azure_second.AzureGroup/azure-second/StorageConnection" />
          </setting>
        </map>
        <map name="Mapazure-secondInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/azure_second.Azure/azure_second.AzureGroup/azure-secondInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="azure-second" generation="1" functional="0" release="0" software="C:\Users\Robocop\Documents\Visual Studio 2013\Projects\azure-second\azure-second.Azure\csx\Debug\roles\azure-second" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="StorageConnection" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;azure-second&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;azure-second&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/azure_second.Azure/azure_second.AzureGroup/azure-secondInstances" />
            <sCSPolicyUpdateDomainMoniker name="/azure_second.Azure/azure_second.AzureGroup/azure-secondUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/azure_second.Azure/azure_second.AzureGroup/azure-secondFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="azure-secondUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="azure-secondFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="azure-secondInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="5b92d549-b541-4fe4-863d-72aad55b8b9e" ref="Microsoft.RedDog.Contract\ServiceContract\azure_second.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="774dcc97-5085-4644-aaee-95aa02413b1f" ref="Microsoft.RedDog.Contract\Interface\azure-second:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/azure_second.Azure/azure_second.AzureGroup/azure-second:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>